package org.exeample.service;

import org.exeample.entity.Book;
import org.exeample.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;

    public List<Book> allUsersObj(){
        List<Book> allBooks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Book book = new Book("Name" + i,"emal@.com" + i);
            allBooks.add(book);
        }

        return allBooks;
    }

//    public List<book> allUsers(){
//        return bookRepository.findAll();
//    }
//    public void saveBook(Book book){
//        bookRepository.save(book);
//    }
//    public void deleteBook(Long bookId){
//        bookRepository.deleteById(bookId);
//    }
}
