package org.exeample.controller;

import org.exeample.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
    @Autowired
    private BookService bookService;

    @GetMapping("/hi")
    public String sayHello() {
        return "hello_world";
    }

    @GetMapping("/all")
    public String addData(Model model){
        model.addAttribute("allUsers", bookService.allUsersObj());
        return "all";
    }
}
